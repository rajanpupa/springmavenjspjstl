<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<jsp:useBean id="test" class="test.TestApplication"></jsp:useBean>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%=test.getIntroduction()%>
	<br />
	
	<a href="/intro">intro</a><br/>
	
	<c:forTokens items="Zara,nuha,roshy" delims="," var="name">
        <c:out value="${name}" /><br/>
    </c:forTokens>
    
</body>
</html>